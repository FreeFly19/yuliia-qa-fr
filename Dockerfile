FROM bamos/openface

RUN pip install klein

WORKDIR /root/openface
ADD . .

CMD python web.py