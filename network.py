import imagehash
import numpy as np
import openface
from PIL import Image
from sklearn.grid_search import GridSearchCV
from sklearn.svm import SVC

imgDim = 96

align = openface.AlignDlib('./models/dlib/shape_predictor_68_face_landmarks.dat')
net = openface.TorchNeuralNet('./models/openface/nn4.small2.v1.t7', imgDim = imgDim)


def landmarks_align_box(rgb_frame, bb):
    landmarks = align.findLandmarks(rgb_frame, bb)
    aligned_face = align.align(imgDim, rgb_frame, bb, landmarks=landmarks,
                               landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE)

    phash = str(imagehash.phash(Image.fromarray(aligned_face)))

    box = {
        'x': rgb_frame.shape[1] - bb.right(),
        'y': bb.top(),
        'width': bb.right() - bb.left(),
        'height': bb.bottom() - bb.top()
    }

    return {
        'rep': net.forward(aligned_face),
        'phash': phash,
        'box': box
    }


def to_numpy_array(img):
    buf = np.fliplr(np.asarray(img))
    rgb_frame = np.zeros((img.height, img.width, 3), dtype=np.uint8)
    rgb_frame[:, :, 0] = buf[:, :, 2]
    rgb_frame[:, :, 1] = buf[:, :, 1]
    rgb_frame[:, :, 2] = buf[:, :, 0]
    return rgb_frame


class Face:
    def __init__(self, label, image_uri, rep, phash):
        self.label = label
        self.imageUri = image_uri
        self.rep = rep
        self.phash = phash

class Network:
    def __init__(self):
        self.folds = 2
        self.svm = GridSearchCV(SVC(C=1, probability=True), [
            {'C': [1, 10, 100, 1000], 'kernel': ['linear']},
            {'C': [1, 10, 100, 1000], 'gamma': [0.001, 0.0001], 'kernel': ['rbf']}
        ], cv=self.folds)

        self.learntFaces = []
        self.newFaces = []
        self.isLearning = False
        self.isFitted = False


    def loadNewFace(self, label, image_uri, img):
        rgb_frame = to_numpy_array(img)
        bb = align.getLargestFaceBoundingBox(rgb_frame)

        if bb is None:
            return False

        res = landmarks_align_box(rgb_frame, bb)

        for f in self.newFaces:
            if f.phash == res['phash']: return

        for f in self.learntFaces:
            if f.phash == res['phash']: return

        self.newFaces.append(Face(label, image_uri, res['rep'], res['phash']))

    def train(self):
        if self.isLearning:
            return False

        if not self.isFitted and len(np.unique([f.label for f in self.newFaces])) < 2:
            self.isLearning = False
            return False

        self.isLearning = True
        self.learntFaces.extend(self.newFaces)
        self.newFaces = []

        X = []
        y = []

        for f in self.learntFaces:
            X.append(f.rep)
            y.append(f.label)

        self.enrich_data_set_with_duplicates(X, y)

        X = np.vstack(X)
        y = np.array(y)

        self.svm.fit(X, y)
        self.isFitted = True
        self.isLearning = False

        return True

    def predict(self, img):
        rgb_frame = to_numpy_array(img)
        face_bounding_boxes = align.getAllFaceBoundingBoxes(rgb_frame)
        faces_lendmarks = map(lambda bb: landmarks_align_box(rgb_frame, bb), face_bounding_boxes)

        if len(faces_lendmarks) == 0:
            return []

        if len(np.unique([f.label for f in self.newFaces])) == 1:
            for r in faces_lendmarks:
                res = [{
                    'id': self.newFaces[0].label,
                    'p': 1
                }]

                res = sorted(res, key=lambda kv: -kv['p'])
                return [{'prob': res, 'box': r['box']}]

        if not self.isFitted:
            return []

        return self.predict_faces(faces_lendmarks)

    def predict_faces(self, results):
        retVal = []
        for r in results:
            res = []
            for i, v in enumerate(self.svm.best_estimator_.classes_):
                res.append({
                    'id': v,
                    'p': self.svm.predict_proba([r ['rep']])[0][i]
                })
            res = sorted(res, key=lambda kv: -kv['p'])
            retVal.append({'prob': res, 'box': r ['box']})

        return retVal

    def enrich_data_set_with_duplicates(self, X, y):
        label_amount = {}
        for l in y:
            label_amount[l] = label_amount[l] if l in label_amount else 1

        for idx, [l, v] in enumerate(label_amount.iteritems()):
            while v < self.folds:
                indices = [i for i, label in enumerate(y) if label == l]

                labels = map(lambda i: y[i], indices)
                values = map(lambda i: X[i], indices)

                y.extend(labels)
                X.extend(values)

                v = v + len(indices)
