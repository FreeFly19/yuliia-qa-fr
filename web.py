import json
import StringIO
import base64

from PIL import Image
from klein import run, route
from twisted.web.static import File

import network

net = network.Network()
users = []

def read_image(image):
    head = "data:image/png;base64,"
    if image.startswith(head):
        imgdata = base64.b64decode(image[len(head):])
    else:
        head = "data:image/jpeg;base64,"
        if image.startswith(head):
            imgdata = base64.b64decode(image[len(head):])
        else:
            assert False

    imgF = StringIO.StringIO()
    imgF.write(imgdata)
    imgF.seek(0)
    return Image.open(imgF)


@route('/api/register', methods = ['POST'])
def register(request):
    request.setHeader('Content-Type', 'application/json')
    content = json.loads(request.content.read(), 'utf-8')

    if len(filter(lambda u: u['login'] == content['login'], users)) > 0:
        return '"User with \\"' + content['login'] +'\\" username already exists."'

    users.append({
        'login': content['login'],
        'password': content['password']
    })

    for p in content['photos']:
        print(p)
        img = read_image(p)
        net.loadNewFace(content['login'], content['login'], img)

    net.train()

    return '"OK"'

@route('/api/login-first', methods = ['POST'])
def loginFirst(request):
    request.setHeader('Content-Type', 'application/json')
    content = json.loads(request.content.read(), 'utf-8')

    if len(filter(lambda u: u['login'] == content['login'] and u['password'] == content['password'], users)) == 0:
        return '"Bad credentials"'

    return '"OK"'

@route('/api/faces/predict', methods=['POST'])
def predict(request):
    request.setHeader('Content-Type', 'application/json')
    content = json.loads(request.content.read(), 'utf-8')
    head = "data:image/png;base64,"
    if content['image'].startswith(head):
        imgdata = base64.b64decode(content['image'][len(head):])
    else:
        head = "data:image/jpeg;base64,"
        if content['image'].startswith(head):
            imgdata = base64.b64decode(content['image'][len(head):])
        else:
            assert False


    imgF = StringIO.StringIO()
    imgF.write(imgdata)
    imgF.seek(0)
    img = Image.open(imgF)

    return json.dumps(net.predict(img))

@route('/clear', methods = ['GET'])
def clear(request):
    global net, users

    request.setHeader('Content-Type', 'application/json')

    net = network.Network()
    users = []

    return '"CLEARED"'

@route('/', branch=True)
def pg_index(request):
    return File('./frontend')

run("0.0.0.0", 8000)